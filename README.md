# FPM Medical Imaging Analysis

## Learning Outcome Outline					
## Pertemuan

No | Learning Outcome | Materi & Asesmen
---|---|---
8 | <ol><li>Mampu Mengenali konsep file Digital imaging </li><li> Membaca file Dicom menggunakan pemograman sederhana </li><li>Mampu memahami Meta Data File Dicom </li><li>Mampu Menidentifikasi kebutuhan informasi data file Dicom  |<ol><li>[Introduction to Python, Google Colabs ](https://colab.research.google.com/drive/1Wbk9jYaqHhZ5f1vDiCWzaJ5TarVpueI3?usp=sharing) </li><li> [Problem set 1]()
9 | <ol><li>Mengenal Proses Teknik Segmentasi Citra</li><li>Mampu mengoperasikan pemograman sederhana untuk segmentasi citra hasi MRI dengan Library Sederhana **Simple ITK** |<ol><li>[Segmentasi Citra I]() </li><li> [Data Set](https://drive.google.com/file/d/1JsqHQPwb-r_4_H4_6vD-3i_GFLE41CAv/view?usp=sharing)
10 | ‌visulasisasi file dicom menggunakan imageIO (axial coronal sagital) <ol><li> Membaca file DICOM menggunakan ImageIO </li><li>Memahani Atribut metada file DICOM </li><li>Mengetahui proses akse atribut file DICOM </li><li>Mengetahui represetansi citra medis menggunakan Matplotlib </li><li>Mampu memahami prose stack dan membaca kumpulan banyak citra medis</li><li>Memahami Pixel Spacing, Shape, Slice Thickness,Aspect Ratio, dan Field of View </li><li>Dapat membuat visualisasi interactive dari file DICOM untuk Merepresentasikan bagian Axial, Coronal dan Sagital menggunakan Ipywidgets|[Materi](https://colab.research.google.com/drive/100RTf3VPrTXkTdtEcqtQGjnVhFul7KdC?authuser=0#scrollTo=TJJdMum3Fk0R)[Asesmen/Tugas]() 
11 | ‌Medical Image Pre-Processing with Python 
13 | Medical imaging analis menggunakan platipy
14 | ‌Atlas segmentation bronchus segmentation
‌15 | Cardiac segmentation dvh analysis
‌16 | Project UAS  


## Tools
- Python
- Google Colabs
- File Dicom 
- Library (Numpy, Matplotlib, ImageIO, Pydicom,Platipy etc.. )

## Referensi



